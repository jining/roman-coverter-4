package roman;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import roman.RomanConverter;

public class RomanConverterTest {

	@Test
	public void testM() {
		assertEquals( "M", RomanConverter.toRoman(1000));
	}
	public void testCM() {
		assertEquals( "CM", RomanConverter.toRoman(900));
	}
	public void testD() {
		assertEquals( "D", RomanConverter.toRoman(500));
	}
	public void tesCD() {
		assertEquals( "CD", RomanConverter.toRoman(400));
	}
	public void testC() {
		assertEquals( "C", RomanConverter.toRoman(100));
	}
	public void testXC() {
		assertEquals( "XC", RomanConverter.toRoman(90));
	}
	public void testL() {
		assertEquals( "L", RomanConverter.toRoman(50));
	}
	public void tesXL() {
		assertEquals( "XL", RomanConverter.toRoman(40));
	}
	public void tesX() {
		assertEquals( "X", RomanConverter.toRoman(10));
	}
	public void testIX() {
		assertEquals( "IX", RomanConverter.toRoman(9));
	}
	public void testV() {
		assertEquals( "V", RomanConverter.toRoman(5));
	}
	public void testIV() {
		assertEquals( "IV", RomanConverter.toRoman(4));
	}
	public void tesI() {
		assertEquals( "I", RomanConverter.toRoman(1));
	}
	public void fromRomanTestI() {
		assertEquals(1, RomanConverter.fromRoman("I"));
	}
	public void fromRomanTestIV() {
		assertEquals(4, RomanConverter.fromRoman("IV"));
	}
	public void fromRomanTestV() {
		assertEquals(5, RomanConverter.fromRoman("V"));
	}
	public void fromRomanTestIX() {
		assertEquals(9, RomanConverter.fromRoman("IX"));
	}
	public void fromRomanTestX() {
		assertEquals(10, RomanConverter.fromRoman("X"));
	}
	public void fromRomanTestXL() {
		assertEquals(40, RomanConverter.fromRoman("XL"));

	}
	public void fromRomanTestL() {
		assertEquals(50, RomanConverter.fromRoman("L"));
	}
	public void fromRomanTestXC() {
		assertEquals(90, RomanConverter.fromRoman("XC"));
	}
	public void fromRomanTestC() {
		assertEquals(100, RomanConverter.fromRoman("C"));
	}
	public void fromRomanTestCD() {
		assertEquals(400, RomanConverter.fromRoman("CD"));
	}
	public void fromRomanTestD() {
		assertEquals(500, RomanConverter.fromRoman("D"));
	}
	public void fromRomanTestCM() {
		assertEquals(900, RomanConverter.fromRoman("CM"));
	}
	public void fromRomanTestM() {
		assertEquals(1000, RomanConverter.fromRoman("M"));
	}
	
	public void fromRomanTestII() {
		assertEquals(2, RomanConverter.fromRoman("II"));
	}
	
}
